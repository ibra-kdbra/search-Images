using Masuit.Tools.Logging;
using Masuit.Tools.Media;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using Masuit.Tools.Files.FileDetector;
using Masuit.Tools.Systems;
using Image = SixLabors.ImageSharp.Image;
using ImageFormat = System.Drawing.Imaging.ImageFormat;
using System.Runtime.InteropServices;

namespace searchByImage {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void btnDirectory_Click(object sender, EventArgs e) {
            using var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK) {
                txtDirectory.Text = dialog.SelectedPath;
            }
        }

        private void btnPic_Click(object sender, EventArgs e) {
            using var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK) {
                txtPic.Text = dialog.FileName;
            }
        }

        private ConcurrentDictionary<string, ulong[]> _index = new();

        public bool IndexRunning { get; set; }

        private async void btnIndex_Click(object sender, EventArgs e) {
            if (IndexRunning) {
                IndexRunning = false;
                btnIndex.Text = "update index";
                return;
            }

            if (string.IsNullOrEmpty(txtDirectory.Text)) {
                MessageBox.Show("Please select a folder first");
                return;
            }

            IndexRunning = true;
            btnIndex.Text = "stop indexing";
            cbRemoveInvalidIndex.Hide();
            var imageHasher = new ImageHasher(new ImageSharpTransformer());
            int? filesCount = null;
            var dir = new DirectoryInfo(txtDirectory.Text);
            _ = Task.Run(() => filesCount = dir.EnumerateFiles("*", SearchOption.AllDirectories).Count(s => Regex.IsMatch(s.Name, "(jpg|png|bmp)$", RegexOptions.IgnoreCase)));
            await Task.Run(() => {
                var sw = Stopwatch.StartNew();
                int pro = 1;
                long size = 0;
                dir.EnumerateFiles("*", SearchOption.AllDirectories).Where(s => Regex.IsMatch(s.Name, "(jpg|png|bmp)$", RegexOptions.IgnoreCase)).Chunk(Environment.ProcessorCount * 2).AsParallel().WithDegreeOfParallelism(Environment.ProcessorCount * 2).ForAll(g => {
                    foreach (var s in g) {
                        if (IndexRunning) {
                            if (lblProcess.InvokeRequired) {
                                lblProcess.Invoke(() => lblProcess.Text = $"{pro++}/{filesCount}");
                            }
                            try {
                                _index.GetOrAdd(s.FullName, _ => imageHasher.DifferenceHash256(s.FullName));
                                size += s.Length;
                            }
                            catch {
                                LogManager.Info(s + "incorrect format");
                            }
                        }
                        else {
                            break;
                        }
                    }
                });
                lbSpeed.Text = $"indexing speed: {Math.Round(pro * 1.0 / sw.Elapsed.TotalSeconds)} items/s({size * 1f / 1048576 / sw.Elapsed.TotalSeconds:N}MB/s)";
                if (cbRemoveInvalidIndex.Checked) {
                    foreach (var (key, _) in _index.AsParallel().WithDegreeOfParallelism(32).Where(s => !File.Exists(s.Key))) {
                        _index.TryRemove(key, out _);
                    }
                }

                lbIndexCount.Text = _index.Count + "document";
                cbRemoveInvalidIndex.Show();
                var json = JsonSerializer.Serialize(_index);
                File.WriteAllText("index.json", json, Encoding.UTF8);
                MessageBox.Show("Index creation is complete, time-consuming:" + sw.Elapsed.TotalSeconds + "s");
            }).ConfigureAwait(false);
            IndexRunning = false;
            btnIndex.Text = "update index";
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(txtPic.Text)) {
                MessageBox.Show("Please select a picture first");
                return;
            }

            if (_index.Count == 0) {
                MessageBox.Show("There is currently no index, please add a folder to create an index before searching");
                return;
            }

            if (!new FileInfo(txtPic.Text).DetectFiletype().MimeType.StartsWith("image")) {
                MessageBox.Show("Not an image file, cannot be retrieved");
                return;
            }

            var sim = (float)numLike.Value / 100;
            var hasher = new ImageHasher();
            var sw = Stopwatch.StartNew();
            var hash = hasher.DifferenceHash256(txtPic.Text);
            var hashs = new ConcurrentBag<ulong[]> { hash };
            using (var image = Image.Load<Rgba32>(txtPic.Text)) {
                var actions = new List<Action>();
                if (cbRotate.Checked) {
                    actions.Add(() => {
                        using var clone = image.Clone(c => c.Rotate(90));
                        var rotate90 = clone.DifferenceHash256();
                        hashs.Add(rotate90);
                    });
                    actions.Add(() => {
                        using var clone = image.Clone(c => c.Rotate(180));
                        var rotate180 = clone.DifferenceHash256();
                        hashs.Add(rotate180);
                    });
                    actions.Add(() => {
                        using var clone = image.Clone(c => c.Rotate(270));
                        var rotate270 = clone.DifferenceHash256();
                        hashs.Add(rotate270);
                    });
                }

                if (cbFlip.Checked) {
                    actions.Add(() => {
                        using var clone = image.Clone(c => c.Flip(FlipMode.Horizontal));
                        var flipH = clone.DifferenceHash256();
                        hashs.Add(flipH);
                    });
                    actions.Add(() => {
                        using var clone = image.Clone(c => c.Flip(FlipMode.Vertical));
                        var flipV = clone.DifferenceHash256();
                        hashs.Add(flipV);
                    });
                }
                Parallel.Invoke(actions.ToArray());
            }

            var list = _index.Select(x => new {
                path = x.Key,
                suitability = hashs.Select(h => ImageHasher.Compare(x.Value, h)).Max()
            }).Where(x => x.suitability >= sim).OrderByDescending(a => a.suitability).ToList();
            lbElpased.Text = sw.ElapsedMilliseconds + "ms";
            if (list.Count > 0) {
                picSource.ImageLocation = txtPic.Text;
                picSource.Refresh();
            }

            dgvResult.DataSource = list;
        }

        private async void Form1_Load(object sender, EventArgs e) {
            lbIndexCount.Text = "loading index...";
            if (File.Exists("index.json")) {
                _index = await JsonSerializer.DeserializeAsync<ConcurrentDictionary<string, ulong[]>>(File.OpenRead("index.json")).ConfigureAwait(false);
                lbIndexCount.Text = _index.Count + "document";
            }
            else {
                lbIndexCount.Text = "Please create an index first";
            }
        }

        private void dgvResult_CellContentClick(object sender, DataGridViewCellEventArgs e) {
        }

        private void dgvResult_CellClick(object sender, DataGridViewCellEventArgs e) {
            picDest.ImageLocation = dgvResult.SelectedCells[0].OwningRow.Cells["path"].Value.ToString();
        }

        private void picSource_LoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e) {
            lbSrcInfo.Text = $"Resolution: {picSource.Image.Width}x{picSource.Image.Height},size: {new FileInfo(picSource.ImageLocation).Length / 1024}KB";
        }

        private void picDest_LoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e) {
            lblDestInfo.Text = $"Resolution: {picDest.Image.Width}x{picDest.Image.Height},size: {new FileInfo(picDest.ImageLocation).Length / 1024}KB";
        }

        private void lblGithub_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            Process.Start(new ProcessStartInfo { FileName = lblGithub.Text, UseShellExecute = true });
        }

        private void txtDirectory_DragEnter(object sender, DragEventArgs e) {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Link : DragDropEffects.None;
        }

        private void txtDirectory_DragDrop(object sender, DragEventArgs e) {
            ((TextBox)sender).Text = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
        }

        private void txtPic_DragEnter(object sender, DragEventArgs e) {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Link : DragDropEffects.None;
        }

        private void txtPic_DragDrop(object sender, DragEventArgs e) {
            ((TextBox)sender).Text = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
        }

        private void buttonClipSearch_Click(object sender, EventArgs e) {
            if (Clipboard.ContainsFileDropList()) {
                txtPic.Text = Clipboard.GetFileDropList()[0];
                btnSearch_Click(sender, e);
                return;
            }

            if (Clipboard.ContainsText()) {
                var text = Clipboard.GetText();
                if (File.Exists(text)) {
                    btnSearch_Click(sender, e);
                }
                else {
                    dgvResult.DataSource = null;
                    picSource.Image = null;
                    picSource.Refresh();
                }
                return;
            }

            if (Clipboard.ContainsImage()) {
                using var sourceImage = Clipboard.GetImage();
                var filename = Path.Combine(Environment.GetEnvironmentVariable("temp"), SnowFlake.NewId);
                sourceImage.Save(filename, ImageFormat.Jpeg);
                var sim = (float)numLike.Value / 100;
                var hasher = new ImageHasher();
                var sw = Stopwatch.StartNew();
                var hash = hasher.DifferenceHash256(filename);
                var hashs = new ConcurrentBag<ulong[]> { hash };
                using (var image = Image.Load<Rgba32>(filename)) {
                    var actions = new List<Action>();
                    if (cbRotate.Checked) {
                        actions.Add(() => {
                            using var clone = image.Clone(c => c.Rotate(90));
                            var rotate90 = clone.DifferenceHash256();
                            hashs.Add(rotate90);
                        });
                        actions.Add(() => {
                            using var clone = image.Clone(c => c.Rotate(180));
                            var rotate180 = clone.DifferenceHash256();
                            hashs.Add(rotate180);
                        });
                        actions.Add(() => {
                            using var clone = image.Clone(c => c.Rotate(270));
                            var rotate270 = clone.DifferenceHash256();
                            hashs.Add(rotate270);
                        });
                    }

                    if (cbFlip.Checked) {
                        actions.Add(() => {
                            using var clone = image.Clone(c => c.Flip(FlipMode.Horizontal));
                            var flipH = clone.DifferenceHash256();
                            hashs.Add(flipH);
                        });
                        actions.Add(() => {
                            using var clone = image.Clone(c => c.Flip(FlipMode.Vertical));
                            var flipV = clone.DifferenceHash256();
                            hashs.Add(flipV);
                        });
                    }
                    Parallel.Invoke(actions.ToArray());
                }

                var list = _index.Select(x => new {
                    path = x.Key,
                    suitability = hashs.Select(h => ImageHasher.Compare(x.Value, h)).Max()
                }).Where(x => x.suitability >= sim).OrderByDescending(a => a.suitability).ToList();
                lbElpased.Text = sw.ElapsedMilliseconds + "ms";
                if (list.Count > 0) {
                    picSource.ImageLocation = filename;
                    picSource.Refresh();
                }

                dgvResult.DataSource = list;
                Task.Run(() => {
                    Thread.Sleep(1000);
                    File.Delete(filename);
                }).ContinueWith(_ => 0).ConfigureAwait(false);
            }
        }

        private void dgvResult_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e) {
            if (e.Button == MouseButtons.Right && dgvResult.SelectedCells.Count > 0) {
                dgvContextMenuStrip.Show(MousePosition.X, MousePosition.Y);
                return;
            }

            dgvContextMenuStrip.Hide();
        }

        private void OpenTheFolder_Click(object sender, EventArgs e) {
            ExplorerFile(dgvResult.SelectedCells[0].OwningRow.Cells["path"].Value.ToString());
        }

        private void dgvResult_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            Process.Start(new ProcessStartInfo { FileName = dgvResult.SelectedCells[0].OwningRow.Cells["path"].Value.ToString(), UseShellExecute = true });
        }

        /// <summary>
        /// Open the path and locate the file...
        /// </summary>
        [DllImport("shell32.dll", ExactSpelling = true)]
        private static extern void ILFree(IntPtr pidlList);

        [DllImport("shell32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        private static extern IntPtr ILCreateFromPathW(string pszPath);

        [DllImport("shell32.dll", ExactSpelling = true)]
        private static extern int SHOpenFolderAndSelectItems(IntPtr pidlList, uint cild, IntPtr children, uint dwFlags);

        public void ExplorerFile(string filePath) {
            if (!File.Exists(filePath) && !Directory.Exists(filePath))
                return;

            if (Directory.Exists(filePath))
                Process.Start(@"explorer.exe", "/select,\"" + filePath + "\"");
            else {
                IntPtr pidlList = ILCreateFromPathW(filePath);
                if (pidlList != IntPtr.Zero) {
                    try {
                        Marshal.ThrowExceptionForHR(SHOpenFolderAndSelectItems(pidlList, 0, IntPtr.Zero, 0));
                    }
                    finally {
                        ILFree(pidlList);
                    }
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
            if (keyData == (Keys.Control | Keys.V)) {
                buttonClipSearch_Click(null, null);
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void label1_Click(object sender, EventArgs e) {

        }

        private void cbRemoveInvalidIndex_CheckedChanged(object sender, EventArgs e) {

        }

        private void lblGithub_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e) {

        }
    }
}
