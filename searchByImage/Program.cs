using Masuit.Tools.Logging;
using System.Diagnostics;

namespace searchByImage;

internal static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
        Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.BelowNormal;

        //Handle uncaught exceptions
        Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

        //Handle UI thread exception
        Application.ThreadException += (sender, e) =>
        {
            LogManager.Error(e.Exception);
            MessageBox.Show(e.Exception.Message);
        };

        //Handle non-UI thread exceptions
        AppDomain.CurrentDomain.UnhandledException += (sender, e) => LogManager.Error((Exception)e.ExceptionObject);

        ApplicationConfiguration.Initialize();
        Application.Run(new Form1());
    }
}
